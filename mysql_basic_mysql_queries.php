<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    
    // Database connection:
    // Connect to the database using MySQLi
    $host = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'gmomysql';   
    $conn = new mysqli($host, $username, $password, $database);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // Inserting Data:
    // Insert data into the employee table
    // SQL query to insert a new employee
    $sql = "INSERT INTO employees (first_name, last_name, middle_name, birthday, address)
    VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    
    // Retrieving a Data:
    // Retrieve the first name, last name, and birthday of all employees in the table
    $sql =  "SELECT first_name, last_name, birthday FROM employees";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }

    // Counting Employees with Last Name Starting with 'D':
    // Retrieve the number of employees whose last name starts with the letter 'D   '
    $sql =  "SELECT COUNT(*) AS count FROM employees WHERE last_name LIKE 'D%'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    
    // Retrieving Employee with Highest ID:
    // Retrieve the first name, last name, and address of the employee with the highest ID number
    $sql =  "SELECT first_name, last_name, address FROM employees WHERE id = (SELECT MAX(id) FROM employees)";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    
    // Updating Data
    // Update data in the employee table
    $sql =  "SELECT first_name, last_name, address FROM employees WHERE id = (SELECT MAX(id) FROM employees)";
    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
    // SQL query to update employee address
    $sql = "UPDATE employees SET address = '123 Main Street' WHERE first_name = 'John'";
    if ($conn->query($sql) === TRUE) {
        echo "Employee address updated successfully";
    } else {
        echo "Error updating employee address: " . $conn->error;
    }

    // Deleting Data:
    // Delete data from the employee table
    $sql = "DELETE FROM employees WHERE last_name LIKE 'D%'";
    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    $conn->close();
    
    ?>
</body>
</html>
